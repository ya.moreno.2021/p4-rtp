## Ejercicio 3. Análisis general
* ¿Cuántos paquetes componen la captura?: 1268
* ¿Cuánto tiempo dura la captura?: 12.810068 segundos
* Aproximadamente, ¿cuántos paquetes se envían por segundo?: 99 paquetes/segundo
* ¿Qué protocolos de nivel de red aparecen en la captura?: IPV4
* ¿Qué protocolos de nivel de transporte aparecen en la captura?: UDP
* ¿Qué protocolos de nivel de aplicación aparecen en la captura?: RTP
## Ejercicio 4. Análisis de un paquete
* Número aleatorio elegido (N): 122
* Dirección IP de la máquina A: 192.168.0.10
* Dirección IP de la máquina B: 216.234.64.16
* Puerto UDP desde el que se envía el paquete: 49154
* Puerto UDP hacia el que se envía el paquete: 54550
* SSRC del paquete: 0x2a173650
* Tipo de datos en el paquete RTP (según indica el campo correspondiente): ITU-T G.711 PCMU (0)
## Ejercicio 5. Análisis de un flujo (stream)
* ¿Cuál es el primer número de tu DNI / NIE?: 5
* ¿Cuántos paquetes hay en el flujo F?: 642 paquetes
* ¿El origen del flujo F es la máquina A o B?: La máquina A
* ¿Cuál es el puerto UDP de destino del flujo F?: 54550, es decir, el de la máquina B
* ¿Cuántos segundos dura el flujo F?: 12,81 segundos
## Ejercicio 6. Métricas del flujo
* ¿Cuál es la diferencia máxima en el flujo?: 31.653000 ms
* ¿Cuál es el jitter medio del flujo?: 12.234262 ms
* ¿Cuántos paquetes del flujo se han perdido?: 0 (0.00 %)
* ¿Cuánto tiempo (aproximadamente) está el jitter por encima de 0.4 ms?: 12.78 segundos
## Ejercicio 7. Métricas de un paquete RTP
* ¿Cuál es su número de secuencia dentro del flujo RTP?: 26589
* ¿Ha llegado pronto o tarde, con respecto a cuando habría debido llegar?: tarde, ya que el valor del skew es -9.81
* ¿Qué jitter se ha calculado para ese paquete?: 12.105831 ms
* ¿Qué timestamp tiene ese paquete?: 9760
* ¿Por qué número hexadecimal empieza sus datos de audio?: 7
## Ejercicio 8. Captura de una traza RTP
* Salida del comando `date`: vie 20 oct 2023 18:32:34 CEST
* Número total de paquetes en la captura: 2884 paquetes
* Duración total de la captura (en segundos): 11.419 segundos
## Ejercicio 9. Análisis de la captura realizada
* ¿Cuántos paquetes RTP hay en el flujo?: 450 paquetes
* ¿Cuál es el SSRC del flujo?: 0xed6f4bba
* ¿En qué momento (en ms) de la traza comienza el flujo?: 5363 ms
* ¿Qué número de secuencia tiene el primer paquete del flujo?: 18861
* ¿Cuál es el jitter medio del flujo?: 0.000000 ms
* ¿Cuántos paquetes del flujo se han perdido?: 0 (0.00 %)
## Ejercicio 10 (segundo periodo). Analiza la captura de un compañero
* ¿De quién es la captura que estás usando?: David Santa Cruz del Moral
* ¿Cuántos paquetes de más o de menos tiene que la tuya? (toda la captura): tiene 29 paquetes menos que yo, el tiene 2855 paquetes y yo 2884 
* ¿Cuántos paquetes de más o de menos tiene el flujo RTP que el tuyo?: tenemos el mismo número de paquetes RTP, es decir, 450 paquetes
* ¿Qué diferencia hay entre el número de secuencia del primer paquete de su flujo RTP con respecto al tuyo?: El mio es 18861 y el suyo es 5290, la diferencia es de 13571
* En general, ¿que diferencias encuentras entre su flujo RTP y el tuyo?: cambia la sequencia de los paquetes, los valores de algunas deltas, los valores del campo de ancho de banda y el tiempo en el que empieza el flujo RTP
